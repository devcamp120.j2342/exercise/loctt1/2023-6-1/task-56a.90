package com.devcamp.s50.task56a90.restapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class RetangleController {
     @GetMapping("/rectangle-area")
     public double getRectangleArea(@RequestParam("width") float width, @RequestParam("length") float length) {
          Rectangle rectangle = new Rectangle(length, width);
          System.out.println(rectangle.toString());
          return rectangle.getArea();
     }

     @GetMapping("/rectangle-perimeter")
     public double getRectanglePerimeter(@RequestParam("width") float width, @RequestParam("length") float length) {
          Rectangle rectangle = new Rectangle(length, width);
          System.out.println(rectangle.toString());
          return rectangle.getPerimeter();
     }
}
